# COVID-19-NGS-PUBLICATION

PENDING PEER-REVIEW

Use of Next-Generation Sequencing for Novel Virus Discovery, Analysis, and Surveillance in the COVID-19 Pandemic and Beyond

Charlie Rubsamen1, Madeleine Brady2, Lydian Frost2, Clare Hoban3, Scott Burkholz1,*

1 Flow Pharma, Inc., Warrensville Heights, OH, USA

2 Dartmouth College, Hanover, NH, USA

3 University of Notre Dame, Notre Dame, IN, USA


Next-Generation Sequencing (NGS) provides the ability to discover a novel viral pathogen through the assembly of the genome and evolutionary relationships to previously characterized genomes. This technology was effectively used during the COVID-19 pandemic to identify a virus of unknown origins, SARS-CoV-2, that spread around the world. Subsequent analysis through NGS monitored the virus for variants of concern, tracked the transmission of dominant variants, and identified targets for pharmaceutical development. An ongoing surveillance program that includes NGS will be necessary for years to come in order to continue these efforts. A review of effective NGS use during the COVID-19 pandemic not only demonstrates paramount successes, but also reveals areas to consider for improvement in the future for a new emerging or re-emerging threat. 
